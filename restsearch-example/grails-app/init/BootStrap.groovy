import restsearch.example.*

import static restsearch.example.Foo.FooStatusEnum.*

class BootStrap {

    def init = { servletContext ->
        // Faz com que as enumerações seja exportadas no JSON e XML apenas pelo NOME
    	grails.converters.JSON.registerObjectMarshaller(Enum, { Enum e -> e.name() })

    	def b = new Bar(name: 'bar')
    	b.save(failOnError: true)

    	50.times { i ->
    		def foo = new Foo(name: "Foo (${i})", age: i, specialDate: (new Date().clearTime() - i), fooStatusEnum: BAD, idxOnBar: i)
    		foo.save(failOnError: true)

    		foo = new Foo(name: "Foo (${i+50})", fooStatusEnum: OK, bar: b, idxOnBar: i)
    		foo.save(failOnError: true)

    	}
    	def b2 = new Bar(name: 'bar2')
    	b2.save(failOnError: true)

    }
    def destroy = {
    }
}
