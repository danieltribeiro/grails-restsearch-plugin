// Place your Spring DSL code here
beans = {
}

// Added by the Rest Search plugin:
beans = {
	"jsonEnumRenderer"(grails.rest.render.json.JsonRenderer, Enumeration)
	"xmlEnumRenderer"(grails.rest.render.xml.XmlRenderer, Enumeration)
	"halEnumRenderer"(grails.rest.render.hal.HalJsonRenderer, Enumeration)
	"jsonEnumCollectionRenderer"(grails.rest.render.json.JsonCollectionRenderer, Enumeration)
	"xmlEnumCollectionRenderer"(grails.rest.render.xml.XmlCollectionRenderer, Enumeration)
	"halEnumCollectionRenderer"(grails.rest.render.hal.HalJsonCollectionRenderer, Enumeration)

	for (domainClass in grailsApplication.domainClasses) {
		int domainClassRnd = new Random().nextInt(100000);
		def className = "${domainClass.shortName}_${domainClassRnd}"
		log.debug "Defining converters for class ${className}"
		 "json${className}Renderer"(grails.rest.render.json.JsonRenderer, domainClass.clazz) {
			 excludes = ["class"]
		 }
		 "xml${className}Renderer"(grails.rest.render.xml.XmlRenderer, domainClass.clazz) {
			 excludes = ["class"]
		 }
		 "hal${className}Renderer"(grails.rest.render.hal.HalJsonRenderer, domainClass.clazz) {
			 excludes = ["class"]
		 }
		 "json${className}CollectionRenderer"(grails.rest.render.json.JsonCollectionRenderer, domainClass.clazz) {
			 excludes = ["class"]
		 }
		 "xml${className}CollectionRenderer"(grails.rest.render.xml.XmlCollectionRenderer, domainClass.clazz) {
			 excludes = ["class"]
		 }
		 "hal${className}CollectionRenderer"(grails.rest.render.hal.HalJsonCollectionRenderer, domainClass.clazz) {
			 excludes = ["class"]
		 }

	}
}
