package restsearch.example

import  grails.plugins.restsearch.*

class BarFooController extends NestedRestSearchController<Foo, Bar>{

	BarFooController() {
		super(Foo, 'bar', Bar)
	}

	BarFooController(boolean readOnly) {
		super(Foo, 'bar', Bar, readOnly)
	}
}
