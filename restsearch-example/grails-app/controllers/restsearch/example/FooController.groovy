package restsearch.example

import grails.plugins.restsearch.*

class FooController extends RestSearchController<Foo>{

	FooController() {
		super(Foo, 'name')
	}

	FooController(boolean readOnly) {
		super(Foo, readOnly, 'name')
	}
}
