package restsearch.example

import grails.plugins.restsearch.*

class BarController extends RestSearchController<Bar>{

	BarController() {
		super(Bar)
	}

	BarController(boolean readOnly) {
		super(Bar, readOnly)
	}
}
