class UrlMappings {

    static mappings = {
		"/bars"(resources: 'bar') {
			"/foos"(resources: 'barFoo') // foo accessible as a nested element
		}
		"/foos"(resources: 'foo') // foo is accessible as a root element

		"500"(controller: 'InternalServerError')
		"404"(controller: 'NotFound')
    }
}
