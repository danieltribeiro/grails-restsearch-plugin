package restsearch.example


class Foo {

	enum FooStatusEnum {OK, BAD}
	String name
	Integer idxOnBar
	Integer age
	Date specialDate
	FooStatusEnum fooStatusEnum

	Bar bar

	static restsearch = [
		id: true,
		name: true,
		age: true,
		specialDate: true,
		status: [field: 'fooStatusEnum'], // query fooStatusEnum as q=status:
		kid: [field: 'age', value: "|2"], // fixed value (accepts negate e.g. q=kid:!)
		older: [field: 'age', formula: {val -> "${val}|"}], // olderThan
		younger: [field: 'age', formula: {val -> "|${val}"}], // youngerThan
		idxOnBar: true,
		'bar.id': true,
		'bar-id': [field: 'bar.id'],
		'barId': [field: 'bar.id'] // may search the bar as bar.id, bar-id, barId or only bar
	]

    static constraints = {
    	name(nullable: false, blank: false, maxSize: 32, unique: true)
    	age(nullable: true)
    	specialDate(nullable: true)
    	fooStatusEnum(nullable: true)
    	bar(nullable: true)
		idxOnBar(nullable: true, unique: 'bar')
    }

    String toString() {name}
}
