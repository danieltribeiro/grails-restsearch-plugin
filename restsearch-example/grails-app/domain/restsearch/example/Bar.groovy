package restsearch.example

class Bar {

	String name

	static restsearch = [
		id: true,
		name: true,
	]

    static constraints = {
    	name(nullable: true, blank: true, maxSize: 32, unique: true)

    }

    String toString() {name}
}
