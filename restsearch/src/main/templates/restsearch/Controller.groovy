<%=packageName ? "package ${packageName}\n\n" : ''%>

import grails.plugins.restsearch.RestSearchController

class ${className}Controller extends RestSearchController<${className}>{

	${className}Controller() {
		super(${className})
	}

	${className}Controller(boolean readOnly) {
		super(${className}, readOnly)
	}
}
