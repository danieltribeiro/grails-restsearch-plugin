README

# Introduction#

* This plugins adds rest and search capability to a grails project
* Grails version > 3.0.8

The search is translated from URL parameter q and executed as a Hibernate Criteria. So any Hibernate Database will work
  
# Installation #

## Set up bintray repo in build.gradle file ##

    repositories {
        mavenLocal()
        maven { url "http://jcenter.bintray.com" }
        maven { url "https://repo.grails.org/grails/core" }
    }

## Declare the dependency ##

    compile "org.grails.plugins:restsearch:1.0"

## or download  ##

The latest version can be downloaded from bintray [ ![Download](https://api.bintray.com/packages/danieltribeiro/plugins/restsearch/images/download.svg) ](https://bintray.com/danieltribeiro/plugins/restsearch/_latestVersion)

# Using #

You can just go to the restsearch-example app and then run-app

# Or create a grails project #

    grails create-app myapp
    cd myapp
    Add the dependencies above

Add special configurations to the project

    grails restsearch-quickstart


Create a domain class. eg.

```
#!groovy

class Foo {
	enum Status {OK, BAD}
	String name
	Status status
	Date date
	static restsearch = [
		id: true,
		name: true,
		date: true,
		status: true
	]
	
}


```

**Why should I need to declare which properties are exposed to search?** This way you can hide parameters like passwords, salary among others to be searched

Generate the restsearch controller

    grails generate-restsearch-controller myapp Foo

Comment the dynamic part in grails-app/controllers/urlMapping.groovy

    /*
    "/$controller/$action?/$id?(.$format)?"{
        constraints {
            // apply constraints here
        }
    }
    */
    
Add the url mapping to foos

    "/foos"(resources: 'foo') // foo is accessible as a root element

foos in plural means accessing all foos.
resources: foo is the controller name FooController.
You need to add one entry for each controller manually because of the pluralization good practice in RESTFULL apis.


Now you can search through query string in url


```
#!html

/foos?q=status:OK

```

# Query String Options #

* q - The search query properties and values
* max - The maximum array size in response (pagination size)
* offset - The first record (pagination position)
* sort - The sort properties (separated by comma)
* order - The sorting orders (separated by comma)
* fields - The fields to be returned (spearated by comma)

# Parameter q #

Parameter q is the most important for search. Here you specify the property/values that should be searched.

    /foos?q=status:OK;age:|3

Above returns all foos with status OK AND age smaller than 3. But you can declare multiple q params and they will be threat as a OR clause for each parameter

    /foos?q=status:OK;age:|3&q=status:BAD

Above will return every foo with a (OK status and age smaller than 3) + (BAD status) 

## Property Separator Token (;) ##

    /foos?q=key1:value1;key2:value2; ...

The ; separates each clause.

## Key/Value separator token (: or =) ##

    /foos?q=status:OK;age:|3

or
    /foo?q=status=OK;age=|3

Both (=) and (:) works as a key/value separator. But in this example we will use only the : as key/value separator.

## Equals ##

The value may be searched as equals To search for a specific value in a property use

    /foos?q=myProperty:myValue

## In ##

To search a value within a list of possible values you should use a comma **,** as the possible values separator

    /foos?q=myProperty:value1,value2,value5

Above: Every foo with myProperty having one of this values 'value1', 'value2 or 'value5'

Ps. It has the same result as

    /foos?q=myProperty:value1&q=myProperty:value2&q=myProperty:value5

## Like ##

The value may be searched like the value. The operator is ** * **

    /foos?q=myProperty:*hasThisPart

Above: Every foo with myProperty ending with 'hasThisPart'

    /foos?q=myProperty:hasThisPart*

Above: Every foo with myProperty staring with 'hasThisPart'

    /foos?q=myProperty:*hasThisPart*

Above: Every foo with myProperty with 'hasThisPart' in the middle

    /foos?q=myProperty:*hasThisPart*andThisOtherToo*

Above: Every foo with myProperty having 'hasThisPart' before the 'andThisOtherToo'

### Range ###

To define a ranged value you should use the range operator **|**  
 
    /foos?q=myProperty:5|25

Above: Every foo with myProperty between 5 and 25 (not including 5 and 25)

    /foos?q=myProperty:5|25,5,25

Above: Every foo with myProperty between 5 and 25 (including 5 and 25)
Obs. The in operator (comma) is used to add the values 5 and 25.

## Any Value ##

    /foos?q=myProperty:*

Above: Every foo with any value on myProperty (if myProperty is blank or null will not be returned)

## Negate ##

To negate a expression use the exclamation mark **!**

    /foos?q=myProperty:!*

Above: Every foo without any value on myProperty (only blank or null values in myProperty will be returned)

### Dates ###

Date formats **You must specify the date formats in application.yml** Below an example list:


```
#!yml

    grails:
        databinding:
            dateFormats:
                - yyyy-MM-dd'T'HH:mm:ssX
                - yyyy-MM-dd'T'HH:mm:ssZ
                - yyyy-MM-dd'T'HH:mm:ssz
                - yyyy-MM-dd'T'HH:mm:ss
                - yyyy-MM-DD HH:mm:ss
                - yyyy-MM-DD HH:mm
                - yyyy-MM-dd
```

Examples:

    2015-31-10
    2015-31-10 23:59:59
    2015-31-10T23:59:59
    2015-31-10T23:59:59Z
    2015-31-10T23:59:59-0300

#### Relative dates to current date ####

Using integers on date porperties means milis from epoch in JAVA

* 0 means new Date(0)
* -1 means new Date(-1)
* 7 means new Date(7) 

Using integers followed by a d on date porperties make them relative to the current date.

* 0d means today
* -1d means yesterday
* 7d means a week from now 

Search with date examples:

    /foos?q=lastUpdated:2015-31-10

Above: Every lastUpdated equals 2015-31-10. You can use the range operator.

    /foos?q=lastUpdated:-7d|0d

Above: Every foo updated in the last 7 days


## Pagination ##

    /foos?max=10&offset=70&sort=lastUpdated,name&order=desc,asc

Above: Get 10 foos starting with the 70th sorted by lastUpdated desc and name asc

## Fields ##

    /foos?fields=id,name

Above: List foos retrieving only the id and name


## Only Metadata ##

Every request to index returns metadata about count, offset, paginationation, etc. Inlcuding *onlyHeader=true* in url parameters ignore the content and respond only the metadata.

Index always send back the response header metadata **X-Total-Count** Indicating the total count for the specific search

There are other metadata information sent back if the have value info

Name              | Desc                   | Example Value
:-----------------|:-----------------------|:---------------:
X-Current-Offset  |Pagination offset       |20
X-Search-Order    |Field Order for sorting |desc,asc
X-Search-Q        |Search Query String     |name:rest*;age:5|15
X-Search-Sort     |Field names for sort    |id,name
X-Total-Count     |Total records in search |40

    /foos?headerOnly=true

Above: Retrieve only the metadata (headers) for the request


## Alias ##

You can create alias for properties, formulas and for fixed values


```
#!groovy


class Bar {

	String name

	static restsearch = [
		id: true,
		name: true,
	]

	static constraints = {
		name(nullable: true, blank: true, maxSize: 32, unique: true)

	}

	String toString() {name}
}



```
	

```
#!groovy

package restsearch.example


class Foo {

	enum FooStatusEnum {OK, BAD}
	String name
	Integer age
	Date specialDate
	FooStatusEnum fooStatusEnum

	Bar bar

	static restsearch = [
		id: true,
		name: true,
		age: true,
		specialDate: true,
		status: [field: 'fooStatusEnum'], // query fooStatusEnum as q=status:
		kid: [field: 'age', value: "|2"], // fixed value (accepts negate e.g. q=kid:!)
		older: [field: 'age', formula: {val -> "${val}|"}], // olderThan
		younger: [field: 'age', formula: {val -> "|${val}"}], // youngerThan
		'bar': [field: 'bar.id'],
		'bar.id': true,
		'bar-id': [field: 'bar.id'],
		'barId': [field: 'bar.id'] // may search the bar as bar.id, bar-id, barId or only bar
	]

    static constraints = {
    	name(nullable: false, blank: false, maxSize: 32, unique: true)
    	age(nullable: true)
    	specialDate(nullable: true)
    	fooStatusEnum(nullable: true)
    	bar(nullable: true)
    }

    String toString() {name}
}


```

    /foos?q=kid

Above: Returns all foos that are kids (age < 2)

    /foos?q=kid:false

Above: Returns all foos that are not kids (age >= 2)

    /foos?q=older:10;younger:15

Above: Returns all foos older than 10 and younger than 15 (11, 12, 13, 14)

    /foos?q=bar:2
    /foos?q=bar.id:2
    /foos?q=bar-id:2
    /foos?q=barId:2

Above: Returns all foos belons to bar 2

# FAQ #

## Why wrapping search terms in parameter Q? ##

     /foos?q=name:rest*;age:|8

instead of accepting parameters like below?

    /foos?name=rest*&age=|8

* Makes easier to capture the entire search string to save, send back to user as response header, to parse, etc..
* Avoid conflict. If you have a property called **max** */foo?max=10* is ambiguous: I may want 10 records or just the foos with field *max = 10*?


# Who do I talk to? #

* danieltribeiro@gmail.com